"""
explore-exercise.py

Author:Jakob Orel
Date: 10-16-20
CSC355 Open Source Development
Week 1

Leon's example from class. Blends contour filter and picture with less color.
This does not include comments as it is just the straight forward example
from class that was copied.

"""


import argparse
from PIL import Image, ImageFilter, ImageEnhance

def main():
    print("Hello world")

    parser = argparse.ArgumentParser()
    parser.add_argument("file_name", type=str, help= "filename")
    parser.add_argument("--weight", type=float, help="weight")
    arguments = parser.parse_args()

    picture = Image.open(arguments.file_name)
    picture = picture.reduce(2)

    enhancer = ImageEnhance.Color(picture)
    picture = enhancer.enhance(0.3).convert("RGB")
    print(picture.mode)
    print(picture.size)
    #picture.show()

    picture_copy = picture.copy()
    picture_copy = picture_copy.convert("L")
    picture_copy = picture_copy.filter(ImageFilter.CONTOUR)

    print(picture_copy.mode)
    print(picture_copy.size)

    threshold = lambda x: 32 if x < 192 else 224
    #Look up table
    lut = [32 if i < 192 else 224 for i in range(256)]
    #point will take a list or a function
    picture_copy = picture_copy.point(threshold, mode="L")
    picture_copy = picture_copy.convert("RGB")
    picture_copy = picture_copy.filter(ImageFilter.SMOOTH)

    #picture_copy.show()

    blended = Image.blend(picture, picture_copy, arguments.weight)
    blended.show()


if __name__ == "__main__":
    main()
