"""
process-nasa.py

Author: Jakob Orel
Date: 10-15-20
CSC355 Open Source Development
Week 1

This program attempts to process an image from the Opportunity Mars
Exploration Rover and Curiosity Rover to make it more visible
using some acquired knowledge of the PIL. This did not work out as well as I
had hoped, but I think it helps slightly with the provided images.

"""
from PIL import Image, ImageFilter, ImageOps, ImageEnhance
import argparse

# Argument required:
# Image: Filename of the image to be passed through processing
parser = argparse.ArgumentParser()
parser.add_argument('image', type=str, help="Image to be processed.")
args = parser.parse_args()

def main(args):
    print("Mars Image Processing")
    image = Image.open(args.image).convert("L")
    image.show()

    # Grayscale images can be difficult to analyze and know how to process the
    # image. The image that is passed is enhanced, equalized, autocontrasted,
    # and colorized. Some images work better than others to be filtered.
    #processed_image = image.filter(ImageFilter.SHARPEN)
    enhancer = ImageEnhance.Sharpness(image)
    processed_image = enhancer.enhance(2.5)
    processed_image = ImageOps.equalize(processed_image)
    processed_image = ImageOps.autocontrast(processed_image,cutoff=0.2, ignore=None)
    processed_image = ImageOps.colorize(processed_image, black=(117, 92, 42), white="white")

    processed_image.show()


if __name__== "__main__":
    main(args)
