"""
explore-filters.py

Author: Jakob Orel
Date: 10-15-20
CSC355 Open Source Development
Week 1

This program explores the different default filters provided by ImageFilter PIL
module. Argparse is used to pass the image name and an index argument
to choose the filter. These ideas are very similar to what Leon showed in class.

"""
import argparse
from PIL import Image, ImageFilter

# Arguments when running program:
# Image: the filename/path of the image to enhance
# --index: the integer value (0-9) to select the default filter to apply
parser = argparse.ArgumentParser()
parser.add_argument('image',type=str, help='Filename of image')
parser.add_argument('--index',type=int, help='Integer to choose filter', default=-1)
args = parser.parse_args()

def main(args):
    print("Working with image filters...")
    picture = Image.open(args.image)
    # Show the original picture
    picture.show()

    # Filter the image based on the index passed in the command line
    if (args.index == 0):
        altered_picture = picture.filter(ImageFilter.BLUR)
    elif (args.index == 1):
        altered_picture = picture.filter(ImageFilter.CONTOUR)
    elif (args.index == 2):
        altered_picture = picture.filter(ImageFilter.DETAIL)
    elif (args.index == 3):
        altered_picture = picture.filter(ImageFilter.EDGE_ENHANCE)
    elif (args.index == 4):
        altered_picture = picture.filter(ImageFilter.EDGE_ENHANCE_MORE)
    elif (args.index == 5):
        altered_picture = picture.filter(ImageFilter.EMBOSS)
    elif (args.index == 6):
        altered_picture = picture.filter(ImageFilter.FIND_EDGES)
    elif (args.index == 7):
        altered_picture = picture.filter(ImageFilter.SHARPEN)
    elif (args.index == 8):
        altered_picture = picture.filter(ImageFilter.SMOOTH)
    elif (args.index == 9):
        altered_picture = picture.filter(ImageFilter.SMOOTH_MORE)
    else:
        # If an invalid index is given, show original picture
        altered_picture = picture

    # Show the altered picture
    altered_picture.show()

if __name__ == "__main__":
    main(args)
