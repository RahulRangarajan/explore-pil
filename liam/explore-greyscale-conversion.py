# Wed, 14 October 2020
# CSC355 Open Source Development
# Liam McCormick

# We talked in class about how you can't just average the RGB values  of a color photo to get
# a grayscale versio  of the image, i wanted to see the difference between using a  weight average,
# average, and the ImageOps greyscale conversion function

from PIL import Image
from PIL import ImageOps
import numpy as np

# open the image and convert to np array for manipulation
image = Image.open("pig.jpg").convert("RGB")
im_arr = np.array(image)

# find the width of 1/3rd of the image
third = image.size[0]//3

# split into a left, middle, and right section
left = im_arr[:,:third]
middle = im_arr[:, third:third*2]
right = im_arr[:, third*2:]

# The nested list comprehensions in the following section are modified versions of some code from a stack overflow
# question about using map() on 2d arrays where one of the people that replied recomended using list comprehensions instead

# convert the left side of the array to a greyscale image using a wighted average
left_im = Image.fromarray(np.array([[((pixel[0]*.3 + pixel[1]*.59 + pixel[2]*.11)//1) for pixel in row] for row in left]))
# convert the middle section to a greyscale image by averageing the RGB components of each pixel
middle_im = Image.fromarray(np.array([[(sum(pixel)//3) for pixel in row] for row in middle]))
# convert the right side of the image to a greyscale image using ImageOps' greyscale method, which takes an image as the only arg and returns a greyscale version of that image
right_im = ImageOps.grayscale(Image.fromarray(right))

# creating a new image and pasting the three thirds side by side for comparison
comparison = Image.new('RGB', image.size)
comparison.paste(left_im, (0,0))
comparison.paste(middle_im, (third, 0))
comparison.paste(right_im, (third * 2, 0))

#show final image
comparison.show()
comparison.save("greyscale.png")