
# explore-numpy.py
# Leon Hannah Tabak
# CSC355 Open Source Development
# 12 October 2020
#
# This program is a template for experiments
# with numpy and PIL images and functions.
# The program produces a NumPy array from a
# PIL Image, changes values within that array,
# and then creates a new Image using that array.

import numpy as np
from PIL import Image

def main():
    print( "Hi!" )
    picture = Image.open( "images/flowers-wall.jpg" )

    # It is more convenient to work with smaller images.
    # Here, we create a new image whose width and height
    # are half of the width and height of the original image.
    picture = picture.reduce( 2 )

    d = np.asarray( picture )
    # We cannot change the original image data,
    # but we can change a copy of the data.
    data = d.copy()

    # Warning! height precedes width in a NumPy array,
    # but width precedes height in an Image
    height, width, depth = data.shape

    # Take a slice of the array.
    # This slice is a rectangle at the center of the array.
    # It contains all elements of the original array except
    # those elements in the top 1/4, the bottom 1/4, the left-most
    # 1/4, and the right-most 1/4.
    slice = data[ (height//4):(3*height//4), (width//4):(3*width//4) ]

    # Divide the red, green, and blue components of the elements in
    # the slice by 2. 
    # This will have the effect of darkening the pixels.
    data[ (height//4):(3*height//4), (width//4):(3*width//4) ] = slice // 2

    # Create a new Image from the changed array.
    picture = Image.fromarray( data )

    print( f'(The photograph is {width} x {height} pixels.' )

    picture.show()

if __name__ == "__main__":
    main()
