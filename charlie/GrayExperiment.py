# Charlie Honigman
# CSC 355
# 10/12/2020
# Week 1 Day 1 Program

# Goal:
# I wanted to compare two methods of converting an image to grayscale,
# obviously the ImageOps method is preferable as it is only 2 lines of code
# and is probably far more optimized than mine. In terms of how they perform visually,
# my approach still clearly has color in it, whereas the ImageOps version is
# much more clearly black and white. I tried several formulas for the conversion,
# but none compared to the ImageOps version.


import numpy as np
from PIL import Image, ImageOps

def main():
    wback = Image.open("images/WRXBack.jpg")

    # Rotate takes a degree and rotates the image that much in the clockwise direction
    wback = wback.rotate(270)

    wprof = Image.open("images/WRXProfile.jpg")
    wprof = wprof.rotate(270)
    wprof = wprof.reduce(2)
    wprof.show()


    # ImageOps method
    # the grayscale method takes an image and converts it to black and white
    convertgraywback = ImageOps.grayscale(wprof)
    convertgraywback.show()

    # The numpy approach is to first represent the image a 3D array
    data = np.asarray(wprof)
    graydata = data.copy()

    # Borrowed these 3 lines from Jacob's piazza post
    # Then set each RGB value to the weighted average
    graydata[:, :, 0] = np.average(graydata, axis=2, weights=[.21, .72, .07])
    graydata[:, :, 1] = np.average(graydata, axis=2, weights=[.21, .72, .07])
    graydata[:, :, 2] = np.average(graydata, axis=2, weights=[.21, .72, .07])

    # Finally convert the array back to an image
    # the fromarray function takes an array and a mode
    # We use the RGB mode here because the array is 3D,
    # if we had put one weighted average value per element
    # into a 2D array, we would use the L mode
    wprofgray = Image.fromarray(graydata, "RGB")

    wprofgray.show()



if __name__ == "__main__":
    main();


