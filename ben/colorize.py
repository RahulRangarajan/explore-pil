# colorize.py by Ben Soderberg
# CSC 355
# Work for Week 1
# October 16, 2020

'''
This program will take the image NightHut.jpg and convert it to
bluescale.
'''
# Importing PIL to do image stuff with and numpy to mess around with the
# pixels in the PIL image
from PIL import Image, ImageOps
import numpy as np
# Importing multiprocess to run my main function. Process will take
# the function and distribute the computation load to multiple cores
# therefore, making my program run faster. In my testing, I've seen times
# decrease by about 30% - 40% (although your milage may vary depending
# on what computer you have).
from multiprocess import Process

def main():
    image = Image.open("NightHut.jpg").convert("L")
    im = ImageOps.colorize(image, black ="blue", white ="white")
    im.show()

if __name__ == '__main__':
    p = Process(target=main, args=())
    p.start()
    p.join()
