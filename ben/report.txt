By Ben Soderberg

I set out to just learn about the PIL library. To do this, I just went through
the documentation, found some functions that looked interesting and tried to
make them work.

I ran into some issues in greyscale.py; it is supposed to invert the colors of
the image! I believe the problem is related to something being unhappy with the
.jpg file type but I am not sure. What's even weirder about this whole situation
is that it doesn't even produce a greyscale image. If you look closely, you'll
see that some of the pixels still have color. Unfortunately, I couldn't figure
this one out.

Another issue I ran into was trying to read the command line with the input()
function. If you want to use input, make sure that it is not inside of a called
function. If it is, then you will run into an EOFError. To get around this, I
took the input in if __name__ == '__main__': and passed the value from the
command line in as an argument for the main function.
