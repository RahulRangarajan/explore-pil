# sharpen.py by Ben Soderberg
# CSC 355
# Work for Week 1
# ctober 16, 2020

'''
This program is just a simple showcase of the sharpness enhancer from the
PIL library. It will ask for an integer number from the command line
and use that number to specify the ammount of sharpness applied to 
NightHut.jpg
'''
from PIL import Image, ImageEnhance

factor = int(input("Enter an integer number for how sharp you want the image (higher means sharper): "))
enhancer = ImageEnhance.Sharpness(Image.open("NightHut.jpg"))
enhancer.enhance(factor).show(f"Sharpness {factor:f}")
imagefilter
