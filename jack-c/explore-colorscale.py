'''
Jack Castiglione, Oct 14
Exploration of "colorscaling", a variation of grayscaling
'''

import numpy as np
from PIL import Image, ImageDraw, ImageFont
import colors


def main():
    #Add a basic image to the file
    pic = Image.open("images/me.jpg").convert("RGBA")

    #Obtain an array for a greyscaled L image
    pic = pic.convert("L")
    picArray = np.asarray(pic)

    #Convert array to colorscaled and to new image using a color
    picArray = colorscale(picArray, colors.red)
    im = Image.fromarray(picArray)

    #Show image
    im.show()

   
def colorscale(imageArray, color):
    #Save image size
    height = imageArray.shape[0]
    width = height = imageArray.shape[1]

    #Create final location
    final = np.ndarray(shape=(height, width, 4), dtype=np.uint8)

    #Perform an operation for each pixel in the image
    for i in range(height):
        for j in range(width):
            #Create an RGB value from the color and grey level
            for k in range(3):
                final[i][j][k] = (imageArray[i][j] / 256) * color[k]
            #Insert alpha value
            final[i][j][3] = color[3]

    return final
#colorscale
    
    
if __name__ == "__main__":
    main()
